#ifndef KMP_H_
#define KMP_H_

#include <string.h>

void computeLPSArray(char *pat, int M, int *lps) {
  int len = 0;

  lps[0] = 0;

  int i = 1;
  while (i < M) {
    if (pat[i] == pat[len]) {
      len++;
      lps[i] = len;
      i++;
    } else {
      if (len != 0) {
        len = lps[len - 1];

      } else {
        lps[i] = 0;
        i++;
      }
    }
  }
}

int kmp_search(char *pat, char *txt) {
  int M = strlen(pat);
  int N = strlen(txt);

  int lps[M];

  computeLPSArray(pat, M, lps);

  int i = 0;
  int j = 0;
  while (i < N) {
    if (pat[j] == txt[i]) {
      j++;
      i++;
    }

    if (j == M) {
      return i - j;
    }

    else if (i < N && pat[j] != txt[i]) {
      if (j != 0)
        j = lps[j - 1];
      else
        i = i + 1;
    }
  }
  return -1;
}

int *kmp_search2(char *pat, char *txt, int *OutPut) {
  int M = strlen(pat);
  int N = strlen(txt);

  int lps[M];

  computeLPSArray(pat, M, lps);

  int i = 0;
  int j = 0;
  while (i < N) {
    if (pat[j] == txt[i]) {
      j++;
      i++;
    }

    if (j == M) {
      OutPut[0] = i - j;
      j = lps[j - 1];
      OutPut[1] = j;
      return OutPut;
    }

    else if (i < N && pat[j] != txt[i]) {
      if (j != 0)
        j = lps[j - 1];
      else
        i = i + 1;
    }
  }
  return NULL;
}

#endif
