/* pandoc - pandoc api
   Copyright (C) 2021-2021 Bishr Ghalil.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* Written by Bishr Ghalil */

#ifndef PANDOC_H_
#define PANDOC_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "asprintf.h"

const char *types[6] = {NULL, "pdf", "org", "html", "md", NULL};

int pd_checktype(const char *type) {
  for (int i = 1; types[i] != NULL; i++) {
    if (strcmp(type, types[i]) == 0) {
      return i;
    }
  }
  return 0;
}

int pd_convmd(const char *infile, const char *type, const char *outname) {
  if (system("which pandoc > /dev/null 2>&1") != 0) {
    fprintf(stderr, "Can't use this option, Pandoc isn't installed.\n");
    return 0;
  }
  if (!pd_checktype(type)) {
    fprintf(stderr,
            "Todome: Type (%s) is not supported.\nRun `todome -h` to check for "
            "supported types.\n",
            type);
    return 0;
  }

  int status;

  char *outfile;
  if (asprintf(&outfile, "%s.%s", outname, type) < 0) {
    fprintf(stderr, "Couldn't create formated string for pandoc output\n");
  }
  char *cmd;
  if (asprintf(&cmd, "pandoc -f markdown %s -o %s", infile, outfile) < 0) {
    fprintf(stderr, "Couldn't create formated string for pandoc command\n");
  }

  status = system(cmd);
  free(outfile);
  free(cmd);
  return !status;
}

#endif
