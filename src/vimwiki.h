#ifndef VIMWIKI_H_
#define VIMWIKI_H_
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include "asprintf.h"
#include "kmp.h"

static char *wiki_path;
static char *wiki_dir_path;
static const char *wiki_dir = "vimwiki";
static const char *wiki_index = "index.wiki";

static void vw_initpath() {
  const char *home = getenv("HOME");
  if (asprintf(&wiki_path, "%s/%s/%s", home, wiki_dir, wiki_index) < 0) {
    fprintf(stderr, "Couldn't create formated string for wiki_path\n");
    exit(1);
  }

  if (asprintf(&wiki_dir_path, "%s/%s", home, wiki_dir) < 0) {
    fprintf(stderr, "Couldn't create formated string for wiki_path\n");
    exit(1);
  }
}

FILE *vw_openwiki(const char *mode) {
  FILE *fd = fopen(wiki_path, mode);
  if (fd == NULL) {
    fprintf(stderr, "Couldn't open file index.wiki , %s\n", strerror(errno));
    exit(1);
  }

  return fd;
}

static int vw_search_wiki(const char *str) {
  int wiki_fd = open(wiki_path, O_RDONLY | O_CREAT, S_IRUSR | S_IWUSR);
  if (wiki_fd == -1) {
    fprintf(stderr, "Couldn't open file index.wiki , %s\n", strerror(errno));
    exit(1);
  }

  struct stat sb;

  if (fstat(wiki_fd, &sb) == -1 || sb.st_size < 5) {
    fprintf(stderr, "Couldn't get file index.wiki status, %s\n",
            strerror(errno));
    close(wiki_fd);
    exit(1);
  }

  char *file =
      (char *)mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, wiki_fd, 0);

  if (file == MAP_FAILED) {
    fprintf(stderr, "Couldn't map file index.wiki, %s\n", strerror(errno));
    close(wiki_fd);
    exit(1);
  }

  int str_len = strlen(str);
  char *string = malloc(sizeof(char *) * str_len);
  strncpy(string, str, str_len);
  int res = kmp_search(string, file);
  close(wiki_fd);
  munmap(file, sb.st_size);
  free(string);
  return res;
}

void vw_make_todo() {
  vw_initpath();
  const char *TODO = "= TODOME =";
  const char *TODO_ITEM = "[[TODOME|Todos from projects]]";
  int TODO_len = strlen(TODO);
  int search_res = vw_search_wiki(TODO);

  FILE *wiki_fd = vw_openwiki("a");
  if (search_res < 0) {
    fprintf(wiki_fd, "\n%s\n", TODO);
    if (fseek(wiki_fd, 0, SEEK_END) == -1) {
      fprintf(stderr, "Couldn't seek file %s, %s\n", wiki_path,
              strerror(errno));
      exit(1);
    }
    search_res = ftell(wiki_fd);
  } else {
    search_res += TODO_len + 1; // Plus the new line charecter
    fseek(wiki_fd, search_res, SEEK_SET);
  }

  if (vw_search_wiki(TODO_ITEM) < 0) {
    fprintf(wiki_fd, TODO_ITEM);
  }
  fclose(wiki_fd);
}

#endif
