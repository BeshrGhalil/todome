/* search - search for regext pattern in a string
   Copyright (C) 2021-2021 Bishr Ghalil.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* Written by Bishr Ghalil */

#ifndef SEARCH_H_
#define SEARCH_H_

#include <fcntl.h>
#include <limits.h>
#include <pcre.h>
#include <sqlite3.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include "asprintf.h"
#include "colors.h"
#include "errors.h"
#include "pandoc.h"
#include "sql.h"
#include "vimwiki.h"

#define OVECCOUNT 30 /* should be a multiple of 3 */
char tmpfname[PATH_MAX] = "tmptodome.md";
FILE *TMP_MD_FILE;
sqlite3 *sr_db;

typedef enum RETURN_STATUS { S_PRINT, S_STORE } RETURN_STATUS;
typedef enum SEARCH_FLAG {
  S_NONE,
  S_NOCOLORS,
  S_COLORS,
} SEARCH_FLAG;

static int eval_priority(const char *line) {
  int p = 0;
  if (line[0] == 'T') {
    for (int i = 4; line[i] == 'O'; i++, p++)
      ;
  } else if (line[0] == 'F') {
    for (int i = 5; line[i] == 'E'; i++, p++)
      ;
  } else if (line[0] == 'B') {
    for (int i = 3; line[i] == 'G'; i++, p++)
      ;
  }

  return p;
}

static int addline(sqlite3 *db, char *line, const char *fname,
                   const int index) {
  int i = index != 0 ? index : 1;

  while (line[i++] != '\n')
    ;
  char *tmp = (char *)malloc(i + 1);
  tmp = strncpy(tmp, line + index, i - index);
  tmp[i - index] = '\0';

  int priority = eval_priority(tmp);
  sql_addline(db, tmp, fname, priority);

  free(tmp);
  return i + 1;
}

static int printline(char *line, const int index) {
  int i = index != 0 ? index : 1;

  while (line[i++] != '\n')
    ;
  char *tmp = (char *)malloc(i + 1);
  tmp = strncpy(tmp, line + index, i - index);
  tmp[i - index] = '\0';
  printf("%s", tmp);
  free(tmp);
  return i + 1;
}

void search(sqlite3 *db, const char *subject, const char *pattern,
            const char *fname, enum RETURN_STATUS R_FLAG, SEARCH_FLAG P_FLAG) {
  int erroffset;
  int ovector[OVECCOUNT];
  int rc;
  int index;
  const char *error;
  char *tmp;
  pcre *re;

  if (!subject) {
    return;
  }
  tmp = (char *)subject;
  re = pcre_compile(pattern, 0, &error, &erroffset, NULL);

  if (re == NULL) {
    errMsg("PCRE compilation failed at offset %d: %s\n", erroffset, error);
    return;
  }

  rc = pcre_exec(re, NULL, tmp, strlen(tmp), 0, 0, ovector, OVECCOUNT);

  while (rc > 0) {
    index = ovector[0];
    if (R_FLAG == S_STORE) {
      tmp += addline(db, tmp, fname, index);
    } else {
      if (P_FLAG == S_NOCOLORS) {
        printf("%s: ", fname);
      } else {
        printf(BOLD FCOLOR_BLUE "%s" RESET ": ", fname);
      }
      tmp += printline(tmp, index);
    }
    rc = pcre_exec(re, NULL, tmp, strlen(tmp), 0, 0, ovector, OVECCOUNT);
  }

  pcre_free(re);

  if (rc == 0) {
    rc = OVECCOUNT / 3;
    errMsg("ovector only has room for %d captured substrings\n", rc - 1);
  }
}

int extract_callback(void *notused, int argc, char **argv, char **azcolname) {
  char *sort_flag = "FILE, PRIORITY DESC";
  if (argv[0][0] == '/' || argv[0][0] == '.') {
    fprintf(TMP_MD_FILE, "* **%s**\n", argv[0]);
    sql_fetch_were(sr_db, "LINE", "FILE", argv[0], sort_flag, extract_callback);
  } else {
    fprintf(TMP_MD_FILE, "\t* [ ] %s", argv[0]);
  }
  // fprintf(TMP_MD_FILE, "\n");
  return 0;
}

void extract(sqlite3 *db, const char *md_type, char *outputname) {

  sr_db = db;
  char *sort_flag = "FILE";
  char *todomefname = "TODOME";

  char *tmpfpath;
  char *home = getenv("HOME");
  errEAssert(home != NULL, "Couldn't get the HOME path");
  errEAssert(asprintf(&tmpfpath, "%s/%s", home, tmpfname) >= 0,
             "Couldn't create formated string for tmp file path");
  sr_db = db;

  TMP_MD_FILE = fopen(tmpfpath, "w");
  errEAssert(TMP_MD_FILE != NULL, "Couldn't make tmp file, %s",
             strerror(errno));

  sql_fetch(sr_db, "FILE", sort_flag, extract_callback);

  fclose(TMP_MD_FILE);

  if (strcmp(md_type, "vimwiki") == 0) {
    vw_make_todo();
    errEAssert(asprintf(&outputname, "%s/%s", wiki_dir_path, todomefname) >= 0,
               "Couldn't create formated string for vim wiki path");
    pd_convmd(tmpfpath, "md", outputname);
    char *mv_cmd;
    errEAssert(asprintf(&mv_cmd, "mv %s/%s.md %s/%s.wiki", wiki_dir_path,
                        todomefname, wiki_dir_path, todomefname),
               "Couldn't create formated string for move command");
    errEAssert(system(mv_cmd) == 0, "Couldn't rename todome.md to todome.wiki");
  } else {
    pd_convmd(tmpfpath, md_type, outputname ? outputname : "todome_output");
  }

  char *rm_tmpf_cmd;
  errEAssert(asprintf(&rm_tmpf_cmd, "rm -rf %s", tmpfpath) >= 0,
             "Couldn't create formated string for removing tmp md file");
  errEAssert(system(rm_tmpf_cmd) == 0,
             "Couldn't remove %s.\nTry removing it manually with sudo",
             tmpfpath);
}

#endif
