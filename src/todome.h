/* todome - search for TODOs FIXMEs and BUGs in source code
   Copyright (C) 2021-2021 Bishr Ghalil.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* Written by Bishr Ghalil */

#ifndef TODOME_H_
#define TODOME_H_
#define _GNU_SOURCE

#include <errno.h>
#include <limits.h>
#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define VERSION "1.13"

#define PROGRAM "todome"
#define HEADER_STYLE "*"
#define LIST_STYLE "-"
#define LINE_SIZE 512

#define RE_PATTERN 100
#define RE_OVECCOUNT 30
#define PRIORITY_MAX 7


static int tm_has_colors(void) {

  int flag;
  WINDOW *mainwin;

  if ((mainwin = initscr()) == NULL) {
    fprintf(stderr, "Error initialising ncurses.\n");
    exit(EXIT_FAILURE);
  }

  flag = has_colors();

  delwin(mainwin);
  endwin();

  return flag;
}

static void tm_task_init(char **task, const char *t, int t_len, char **task_ch,
                         const char *ch, int ch_len) {
  *task = (char *)malloc(sizeof(*task) * (t_len + 1));
  *task = strcpy(*task, t);
  *task_ch = (char *)malloc(sizeof(*task) * (ch_len + 1));
  *task_ch = strcpy(*task_ch, ch);
}

static const char *const usage[] = {
    PROGRAM " [options]",
    NULL,
};

static char *tm_repattern_init(char *repattern, const char *task,
                               const char *task_ch) {
  repattern = strcat(repattern, task);
  repattern = strcat(repattern, "(");
  repattern = strcat(repattern, task_ch);
  repattern = strcat(repattern, "*):");
  return repattern;
}

#endif
