/* todome - search for TODOs FIXMEs and BUGs in source code
   Copyright (C) 2021-2021 Bishr Ghalil.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* Written by Bishr Ghalil */

// TODO: better argparsing
// TODOO: better export to vim wiki
#define _DEFAULT_SOURCE
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <sqlite3.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include "argparse.h"
#include "colors.h"
#include "errors.h"
#include "mime.h"
#include "recdir.h"
#include "search.h"
#include "todome.h"

char *repattern;
char *error;
char *task;
char *task_ch;

int version_flag = 0;
int hidden_flag = 0;
int sigint_flag = 0;
int fixme_flag = 0;
int bug_flag = 0;
char *sort_flag;
int all_flag = 0;
int stop_colors = 0;
int SUPPORT_COLORS = 1;
char *dir_path;
char *extract_flag;
char *output_flag;

int files_number = 0;

sqlite3 *tm_db;
char *err_msg = 0;
SEARCH_FLAG S_FLAG = S_COLORS;

void tm_cleanup() {
  if (task) {
    free(task);
  }

  if (task_ch) {
    free(task_ch);
  }

  if (repattern) {
    free(repattern);
  }
}

void tm_sighandler(int sig) {
  (void)sig;
  sigint_flag = 1;
  tm_cleanup();
  errExit("Interrupted");
  return;
}

int tm_arginit(int argc, char **argv) {

  struct argparse_option options[] = {
      OPT_HELP(),
      OPT_GROUP("Arguments"),
      OPT_STRING('p', "path", &dir_path,
                 "Directory path, Current directory is the default", NULL, 0,
                 0),
      OPT_GROUP("Search options"),
      OPT_BOOLEAN('u', "hidden", &hidden_flag, "Search hidden folders", NULL, 0,
                  0),
      OPT_GROUP("Output options"),
      OPT_BOOLEAN('f', "fixme", &fixme_flag, "Search for FIXME", NULL, 0, 0),
      OPT_BOOLEAN('b', "bug", &bug_flag, "Search for BUG", NULL, 0, 0),
      OPT_BOOLEAN('a', "all", &all_flag, "Search for all", NULL, 0, 0),
      OPT_STRING('s', "sort", &sort_flag,
                 "Sort by [[file, priority] [asc, desc]]", NULL, 0, 0),
      OPT_BOOLEAN('c', "colors", &stop_colors, "Don't print colors", NULL, 0,
                  0),
      OPT_STRING('x', "extract", &extract_flag,
                 "Extract results to a [pdf,org,html,md] file", NULL, 0, 0),

      OPT_STRING('o', "output", &output_flag, "Output name when exracting",
                 NULL, 0, 0),
      OPT_GROUP("Info options"),
      OPT_BOOLEAN('v', "version", &version_flag, "Version", NULL, 0, 0),
      OPT_END(),
  };

  struct argparse argparse;
  argparse_init(&argparse, options, usage, 2);
  argparse_describe(&argparse, "\nSearch for TODOs, FIXMEs, BUGs in projects.",
                    "\nBishr Ghalil.");

  argc = argparse_parse(&argparse, argc, (void *)argv);

  if (version_flag) {
    printf("Version: %s\n", VERSION);
    exit(0);
  }

  if (!dir_path) {
    dir_path = (char *)malloc(sizeof(*dir_path) * 2);
    dir_path = strcpy(dir_path, ".");
  }

  if (fixme_flag) {
    tm_task_init(&task, "FIXME", 5, &task_ch, "E", 1);
  } else if (bug_flag) {
    tm_task_init(&task, "BUG", 3, &task_ch, "G", 1);
  } else if (all_flag) {
    tm_task_init(&task, "(TODO|FIXME|BUG)", strlen("(TODO|FIXME|BUG)"),
                 &task_ch, "(O|E|G)", strlen("(O|E|G)"));
  } else {
    tm_task_init(&task, "TODO", 4, &task_ch, "O", 1);
  }

  if (stop_colors) {
    SUPPORT_COLORS = 0;
  }

  if (output_flag && !extract_flag) {
    errExit("Can't use this option without the extract option\nExample: todome "
            "-x pdf -o MyTodoThisMonth.pdf");
  }

  return argc;
}

int main(int argc, char **argv) {
  sigaction(SIGINT, &(struct sigaction){.sa_handler = tm_sighandler}, NULL);

  int rc = sqlite3_open(":memory:", &tm_db);

  if (errAssert(rc == SQLITE_OK, "Couldn't create database: %s\n",
                sqlite3_errmsg(tm_db))) {

    sqlite3_close(tm_db);

    terminate(1);
  }

  sql_create_table(tm_db);

  if (errAssert(rc == SQLITE_OK, "SQL: %s", err_msg)) {

    sqlite3_free(err_msg);
    sqlite3_close(tm_db);

    terminate(1);
  }

  repattern = (char *)malloc(sizeof(*repattern) * RE_PATTERN);
  if (errAssert(repattern != NULL, "Can't allocate memory\n")) {
    sqlite3_close(tm_db);

    terminate(1);
  }

  SUPPORT_COLORS = tm_has_colors();
  tm_arginit(argc, argv);

  if (sort_flag || extract_flag) {
    errEAssert(system("which sqlite3 > /dev/null 2>&1") == 0,
               "Can't use this option, sqlite3 isn't installed.");
  }

  if (SUPPORT_COLORS == 0) {
    S_FLAG = S_NOCOLORS;
  }

  strncpy(repattern, "", 1);
  repattern = tm_repattern_init(repattern, task, task_ch);

  RECDIR *recdir = recdir_open(dir_path);
  if (errAssert(recdir != NULL, "Please use a valid directory path.")) {
    sqlite3_close(tm_db);

    terminate(1);
  }

  errno = 0;
  struct dirent *ent;
  char *file;
  while ((ent = recdir_read(recdir, hidden_flag))) {

    char *path = join_path(recdir_top(recdir)->path, ent->d_name);
    int fd = open(path, O_RDONLY, S_IRUSR | S_IWUSR);
    if (fd == -1) {
      errno = 0;
      free(path);
      continue;
    }

    files_number++;
    struct stat sb;

    if (fstat(fd, &sb) == -1 || sb.st_size < 5) {
      errno = 0;
      close(fd);
      free(path);
      continue;
    }

    file = mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, fd, 0);

    if (file == MAP_FAILED) {
      errno = 0;
      close(fd);
      free(path);
      continue;
    }

    if (!is_text(file)) {
      free(path);
      munmap(file, sb.st_size);
      close(fd);
      continue;
    }

    if (!sort_flag && !extract_flag) {

      search(tm_db, file, repattern, path, S_PRINT, S_FLAG);
      munmap(file, sb.st_size);
      close(fd);
      if (errAssert(errno == 0, "file [%s] %s", path, strerror(errno))) {
        sqlite3_close(tm_db);
        terminate(1);
      }

      free(path);
      continue;
    }

    search(tm_db, file, repattern, path, S_STORE, S_FLAG);

    munmap(file, sb.st_size);
    close(fd);

    if (errAssert(errno == 0, "file [%s] %s", path, strerror(errno))) {
      sqlite3_close(tm_db);
      terminate(1);
    }

    free(path);
  }

  if (extract_flag) {
    extract(tm_db, extract_flag, output_flag ? output_flag : NULL);
  }
  if (sort_flag) {
    sql_fetch(tm_db, "all", sort_flag, sql_print_callback);
  }

  if (errAssert(errno == 0, "%s", strerror(errno))) {
    sqlite3_close(tm_db);
    terminate(1);
  }
  recdir_close(recdir);

  tm_cleanup();
  sqlite3_close(tm_db);

  return EXIT_SUCCESS;
}
