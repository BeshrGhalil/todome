/* mime - check for file types
   Copyright (C) 2021-2021 Bishr Ghalil.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* Written by Bishr Ghalil */


#ifndef MIME_H_
#define MIME_H_

#include <ctype.h>
#include <stdio.h>

int is_text(const char *file) {

  for (int i = 0; i < 20; i++) {
    if ((!isascii(file[i]) || iscntrl(file[i])) && !isspace(file[i])) {
      return 0;
    }
  }

  return 1;
}

#endif
