#ifndef TODOME_SQL_H_
#define TODOME_SQL_H_

#include "asprintf.h"
#include "colors.h"
#include "errors.h"

#include <sqlite3.h>
#include <stdio.h>

enum SORT_KEY { SORT_PRIORITY, SORT_FILE };

static void sql_create_table(sqlite3 *db) {
  char *err_msg;

  char *sql = "DROP TABLE IF EXISTS Matches;"
              "CREATE TABLE Matches("
              "ID INTEGER PRIMARY KEY AUTOINCREMENT,"
              "LINE TEXT NOT NULL,"
              "PRIORITY INTEGER,"
              "FILE TEXT NOT NULL);"
              "CREATE INDEX IF NOT EXISTS idx_file ON Matches (FILE);"
              "CREATE INDEX IF NOT EXISTS idx_priority ON Matches (PRIORITY);";

  int rc = sqlite3_exec(db, sql, 0, 0, &err_msg);

  if (rc != SQLITE_OK) {
    errMsg("SQL error: %s\n", err_msg);
    sqlite3_free(err_msg);
    exit(1);
  }
}

static void sql_addline(sqlite3 *db, const char *line, const char *file,
                        int priority) {
  sqlite3_stmt *res;
  char *sql = "INSERT OR REPLACE INTO Matches (ID, LINE, FILE, PRIORITY)"
              "VALUES ((SELECT ID FROM Matches WHERE LINE = ?), "
              "?, ?, ?); ";

  int rc = sqlite3_prepare_v2(db, sql, -1, &res, 0);

  if (rc != SQLITE_OK) {

    errMsg("Failed to insert data: %s", sqlite3_errmsg(db));
    sqlite3_close(db);

    return;
  }

  sqlite3_bind_text(res, 1, line, -1, 0);
  sqlite3_bind_text(res, 2, line, -1, 0);
  sqlite3_bind_text(res, 3, file, -1, 0);
  sqlite3_bind_int(res, 4, priority);

  rc = sqlite3_step(res);

  sqlite3_finalize(res);
}

int sql_print_callback(void *notused, int argc, char **argv, char **azcolname) {
  if (SUPPORT_COLORS) {
    printf(BOLD FCOLOR_BLUE "%s" RESET ":"
                            " %s",
           argv[3], argv[1] ? argv[1] : "null");
  } else {
    printf("%s: %s", argv[3], argv[1]);
  }
  return 0;
}

void sql_fetch(sqlite3 *db, const char *col, const char *sort_key,
               int func(void *, int, char **, char **)) {
  char *err_msg = 0;
  char *sqlformated = "SELECT DISTINCT %s FROM Matches ORDER BY %s;";
  char *sql;

  if (asprintf(&sql, sqlformated, strcmp(col, "all") == 0 ? "*" : col,
               sort_key) < 0) {
    errMsg("Couldn't create formated string");
  }

  int rc = sqlite3_exec(db, sql, func, 0, &err_msg);
  free(sql);

  if (rc != SQLITE_OK) {
    errMsg("SQL error: %s\n", err_msg);
    sqlite3_free(err_msg);
    exit(1);
  }
}

void sql_fetch_were(sqlite3 *db, const char *col, const char *condition_col,
                    const char *condition_val, const char *sort_key,
                    int func(void *, int, char **, char **)) {
  char *err_msg = 0;
  char *sqlformated =
      "SELECT DISTINCT %s FROM Matches WHERE %s=\'%s\' ORDER BY %s;";
  char *sql;

  if (asprintf(&sql, sqlformated, strcmp(col, "all") == 0 ? "*" : col,
               condition_col, condition_val, sort_key) < 0) {
    errMsg("Couldn't create formated string");
  }

  int rc = sqlite3_exec(db, sql, func, 0, &err_msg);
  free(sql);

  if (rc != SQLITE_OK) {
    errMsg("SQL error: %s\n", err_msg);
    sqlite3_free(err_msg);
    exit(1);
  }
}

#endif
