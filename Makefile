CC=gcc
CFLAGS=-Wall -std=gnu99
LIBS=-lpcre -lncurses -lm -lsqlite3
SRC=src/todome.c src/recdir.c src/argparse.c src/asprintf.c
OBJ=${SRC:.c=.o}
OUTPUT=todome

all: todome

asprintf.o: src/asprintf.h
todome.o: src/argparse.h src/search.h src/pandoc.h src/mime.h src/errors.h src/todome.h src/sql.h
argparse.o: src/argparse.h
recdir.o: src/recdir.h

%.o: %.c
	$(CC) $(CFLAGS) -c $^ -o $@

todome: $(OBJ)
	$(CC) $(CFLAGS) $^ $(LIBS) -o $@

install: all
	cp $(OUTPUT) /usr/bin/$(OUTPUT)

uninstall:
	rm -rf /usr/bin/$(OUTPUT)

clean:
	rm -rf $(OUTPUT) src/*.o

.PHONY: clean install uninstall all
