# Todome

Language agnostic tool that collects TODOs, FIXMEs and BUGs in the source code. 

Urgency of "TODO", "FIXME" and "BUG" is indicated by repetitions of the final character.
For example, one might write FIXMEEEEEEEE for an important issue. Output can be sorted by priority, file, (asc, desc).

## Install
```
git clone https://github.com/BishrGhalil/todome.git
cd todome
sudo make install
```
--- 
#### Arch linux
Arch linux users can install it from the [aur](https://aur.archlinux.org/packages/todome-git/).

Or using an aur helper such as `yay, paru, ...etc`.
```
yay -S todome-git
```

## Dependencies
* [sqlite3](https://sqlite.org/download.html)
* [ncurses](https://invisible-island.net/ncurses/announce.html)
* [pandoc](https://pandoc.org/installing.html) [optional], If you want to use the `-x` flag

## Usage

```
Usage: todome [options]

Search for TODOs, FIXMEs, BUGs in projects.

    -h, --help            show this help message and exit
    
Search options
    -p, --path=<str>      Directory path, Current directory is the default
    -u, --hidden          Search hidden folders

Output options
    -f, --fixme           Search for FIXME
    -b, --bug             Search for BUG
    -a, --all             Search for all
    -s, --sort=<str>      Sort by [[file, priority] [asc, desc]]
    -c, --colors          Don't print colors
    -x, --extract=<str>   Extract results to a [pdf,org,html,md, vimwiki] file
    -o, --output=<str>    Output name when exracting

Info options
    -v, --version         Version

Bishr Ghalil.
```

## Examples

To search for TODOs in `~/Projects/sample`, Then sort results by priority
```
todome -p ~/Projects/sample -s "priority desc, file"
```
To search for FIXMEs in `~/Projects/sample`, Then extract results to Vimwiki
```
todome -f ~/Projects/sample -x vimwiki
```
